##El patron de diseño estructural decorator añade nuevas funciones a los objetos
##En este ejercio refactorice un ejercicio que realizamos en el curso de python sobre convertir segundos a dias, horas, minutos y segundos
##La funcion decorador se le pasa un argumento, lo convierte a milisegundos, se llama la funcion e imprime el valor equivalente a milisegundos
##En este caso, se modifica la funcion para que tambien imprima el equivalente del argumento en milisegundos
def decorator(arg1):
    def wrap(func):
        def wrapped_func(*args):
            milisegundos=arg1*1000
            func(*args)
            print(arg1," equivale a ",milisegundos)
        return wrapped_func
    return wrap

@decorator(4562000)
def ex_2_4(seg):
  dias=seg/86400
  modias=seg%86400
  horas=modias/3600
  modhoras=modias%3600
  minuto=modhoras/60
  modminuto=modhoras%60
  segundo=modminuto
  dias=int(dias)
  horas=int(horas)
  minuto=int(minuto)
  print(seg, " segundos equivale a ", dias, " dias ", horas, " horas ", minuto, " minutos ", segundo, " segundos")

  
ex_2_4(45620000)
