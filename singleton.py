##El patron de diseño creacional Singleton garantiza que solo exista una instancia de la clase, proporcionando un solo punto de acceso
##En esta clase Singleton instanciamos un objeto con dos atributos course, nombre del curso, time, hora del curso
##Se imprime la instancia y valor de la clase
##La primera vez que se crea la clase, inicializa la clase, el resto de veces la ignora 

class Singleton(object):

    __instance = None
    course = None
    time=None

    def __str__(self):
        return repr(self) + ' ' + self.course+' '+self.time

    def __new__(cls):
        if Singleton.__instance is None:
            Singleton.__instance = object.__new__(cls)
        return Singleton.__instance

##Al imprimir nos muestra la instacia de la clase, la cual siempre es la misma
arquitecture = Singleton()
arquitecture.course = "Arquitectura Python"
arquitecture.time="12:00 pm"
#Imprime los valores pasado a arquitecture
print(arquitecture)

django = Singleton()
django.course="Django Basico"
django.time="4:00 pm"
#Imprime los valores pasados a django con la misma instancia
print(django)

#Sigue imprimiendo los valores de django con la misma instancia
print(django)
print(arquitecture)

##Realice una prueba quitando funcion __new__ me imprimia los valores, con dos intancias diferentes
##La segunda vez de imprimir arquitecture me mostraba su valor asignado por primera vez 